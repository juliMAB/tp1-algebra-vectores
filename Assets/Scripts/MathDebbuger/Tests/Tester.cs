﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathDebbuger;
using CustomMath;

public class Tester : MonoBehaviour
{
    void Start()
    {
        Vector3 cero = new Vector3();
        List<Vector3> vectors = new List<Vector3>();
        //toda esta lista de vectores es el vector rojo.
        vectors.Add(new Vec3(10.0f, 0.0f, 0.0f));
        vectors.Add(new Vec3(10.0f, 10.0f, 0.0f));
        vectors.Add(new Vec3(20.0f, 10.0f, 0.0f));
        vectors.Add(new Vec3(20.0f, 20.0f, 0.0f));
        vectors.Add(new Vec3(-30.0f, 20.0f, 0.0f));
        //---------------------------------------------
        Vector3Debugger.AddVectorsSecuence(vectors, false, Color.red, "secuencia");
        Vector3Debugger.EnableEditorView("secuencia");
        //------------------------------------------------
        Vector3Debugger.AddVector(new Vector3(10, 10, 0), Color.blue, "elAzul");
        Vector3Debugger.EnableEditorView("elAzul");
        Vector3Debugger.AddVector(Vector3.down * 7, Color.green, "elVerde");
        Vector3Debugger.EnableEditorView("elVerde");

        //para testear
        Vector3 amarillo2 = new Vector3(10, -20);
        Vector3Debugger.AddVector(new Vector3(10, -20,0),Color.black, "lol");
        Vector3Debugger.UpdatePosition("lol", cero, amarillo2);
        //esto muestra el vector y lo mueve con ese vector3.down
        //Vector3Debugger.AddVector(Vector3.down * -7, Color.yellow, "Amarillo");
        //Amarillo2 = new Vector3(10, -20);
        //tanto en la linea anterior como en esta se le asigna un nombre para despues poder modificarlo en otros lugares.
        //Vector3Debugger.EnableEditorView("Amarillo");
    }

    // Update is called once per frame
    void Update()
    {
       
        Debug.Log("HELLO");
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine( UpdateBlueVector());
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            Vector3Debugger.TurnOffVector("elAzul");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Vector3Debugger.TurnOnVector("elAzul");
        }
        //Test personal.
        if (Input.GetKeyDown(KeyCode.M))
        {
            Vector3 cero = new Vector3();
            Vector3 amarillo2 = new Vector3(10, -20);

            Vector3Debugger.UpdatePosition("lol", cero, amarillo2);
        }

        
    }

    IEnumerator UpdateBlueVector()
    {
        for (int i = 0; i < 100; i++)
        {
            Vector3Debugger.UpdatePosition("elAzul", new Vector3(2.4f, 6.3f, 0.5f) * (i * 0.05f));
            yield return new WaitForSeconds(0.2f);
        }
    }

}
